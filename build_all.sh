#!/usr/bin/env bash

mkdir -p public
mkdir -p docs/pinouts

./etc/generate_tarballs.sh
./etc/write_index.py

doxygen ./docs/doxygen.conf
